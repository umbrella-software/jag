﻿using System;
using JAG.Utility;
using SciterSharp;
using SciterSharp.Interop;

namespace JAG
{
    public class Host : SciterHost
    {
        public Host(SciterWindow window)
        {
            SetupWindow(window);
            AttachEvh(new EventHost());
        }

        /// <summary>
        /// Automatically loads all paths that are prefixed with app:// from the assembly resources
        /// </summary>
        protected override SciterXDef.LoadResult OnLoadData(SciterXDef.SCN_LOAD_DATA sld)
        {
            if (sld.uri.StartsWith("app://"))
            {
                Byte[] data = ResourceUtility.GetResource(sld.uri.Substring("app://".Length));
                SciterX.API.SciterDataReady(sld.hwnd, sld.uri, data, (UInt32) data.Length);
            }

            return base.OnLoadData(sld);
        }
    }
}
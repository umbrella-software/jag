﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using cmd;
using cmd.Commands;
using cmd.Runner;
using cmd.Runner.Shells;

namespace JAG.Utility
{
    public static class ProcessRunner
    {
        /// <summary>
        /// Runs a program using a path to an exe file
        /// </summary>
        public static dynamic Create(String name, params String[] env)
        {
            IRunner runner = Shell.Default;
            Dictionary<String, String> envDictionary = new Dictionary<String, String>();
            for (Int32 i = 0; i < env.Length; i++)
            {
                String[] split = env[i].Split(new []{'='}, 2);
                envDictionary.Add(split[0], split[1]);
            }

            runner.EnvironmentVariables = envDictionary;
            
            ICommando command = runner.GetCommand();
            command.AddCommand(name);
            return command;
        }
    }
}
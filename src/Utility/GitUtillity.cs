﻿using System;
using System.IO;

namespace JAG.Utility
{
    public class GitUtillity
    {
        /// <summary>
        /// Gets the path for a git commandline
        /// </summary>
        /// <returns></returns>
        public static String GetGitPath()
        {
            // Check if the application includes a git
            String path = "";
            #if WINDOWS
            String internalGitPath = Path.Combine(Program.ApplicationPath, "git", "bin", "git.exe");
            #elif LINUX
            String internalGitPath = Path.Combine(Program.ApplicationPath, "git", "bin", "git");
            #endif
            if (File.Exists(internalGitPath))
            {
                // Use the embedded git version
                path = internalGitPath;
            }
            
            // Check if there is a system-wide installation of git
            try
            {
                dynamic globalGit = ProcessRunner.Create("git", "LANG=en_US.UTF-8");
                Version globalVersion = ParseVersion(globalGit(version: true));
                
                // If there is also an embedded git, compare the two versions
                // If not, use the global one
                if (File.Exists(internalGitPath))
                {
                    dynamic internalGit = ProcessRunner.Create(internalGitPath, "LANG=en_US.UTF-8");
                    Version internalVersion = ParseVersion(internalGit(version: true));

                    // Compare the two versions
                    if (internalVersion < globalVersion)
                    {
                        // The global git has won the election, so we replace the path with the global alias
                        path = "git";
                    }
                }
                else
                {
                    // Use the global git version
                    path = "git";
                }
            }
            catch
            {
                // If the try / catch fails, it is likely that no global git version is installed
                // so we fall back to the embedded one
            }

            return path;
        }
        
        public static Version ParseVersion(String input)
        {
            input = input.Substring("git version ".Length);
            String[] split = input.Split('.');
            return new Version(Int32.Parse(split[0]), Int32.Parse(split[1]), Int32.Parse(split[2]));
        }
    }
}
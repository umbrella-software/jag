﻿using System;
using System.IO;
using System.Text;

namespace JAG.Utility
{
    /// <summary>
    /// Various tools for handling assembly resources
    /// </summary>
    public static class ResourceUtility
    {
        /// <summary>
        /// Extracts a file from the assembly resources and returns it as a byte array
        /// </summary>
        public static Byte[] GetResource(String name)
        {
            // Build the resource path
            String resource = "JAG.Resources." + name.Replace("/", ".");
            
            // Access the assembly resource
            using (Stream stream = typeof(ResourceUtility).Assembly.GetManifestResourceStream(resource))
            {
                if (stream == null)
                {
                    throw new ArgumentException(nameof(name));
                }

                using (MemoryStream buffer = new MemoryStream())
                {
                    stream.CopyTo(buffer);
                    return buffer.ToArray();
                }
            }
        }
        /// <summary>
        /// Extracts a file from the assembly resources and returns it as a string
        /// </summary>
        public static String GetResourceString(String name)
        {
            return GetResourceString(name, Encoding.UTF8);
        }
        
        /// <summary>
        /// Extracts a file from the assembly resources and returns it as a string
        /// </summary>
        public static String GetResourceString(String name, Encoding encoding)
        {
            return encoding.GetString(GetResource(name));
        }
    }
}
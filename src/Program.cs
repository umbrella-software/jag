﻿using System;
using System.IO;
using JAG.Utility;
using SciterSharp;
using SciterSharp.Interop;

namespace JAG
{
    /// <summary>
    /// The entrypoint for the application
    /// </summary>
    public class Program
    {
        public static String ApplicationPath
        {
            get { return Path.GetDirectoryName(typeof(Program).Assembly.CodeBase); }
        }
        
        /// <summary>
        /// The path to the git application that is used
        /// </summary>
        public static String GitPath { get; set; }
        
        [STAThread]
        public static void Main(String[] args)
        {
            // Init Sciter
            #if WINDOWS
            PInvokeWindows.OleInitialize(IntPtr.Zero);
            #elif LINUX
            PInvokeGTK.gtk_init(IntPtr.Zero, IntPtr.Zero);
            #endif
            
            // Init Git
            GitPath = GitUtillity.GetGitPath();
            
            // Open the window
            SciterWindow window = new SciterWindow();
            window.CreateMainWindow(800, 600);
            window.CenterTopLevelWindow();
            window.Title = "Test";
            Host host = new Host(window);
            window.LoadPage("app://index.html");
            window.Show();
            
            // Run the application
            #if !OSX
            PInvokeUtils.RunMsgLoop();
            #endif
        }
    }
}
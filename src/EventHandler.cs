﻿using System;
using System.Linq;
using JAG.Utility;
using SciterSharp;

namespace JAG
{
    public class EventHost : SciterEventHandler
    {
        // The methods of this type are exposed to the TIScript code in the templates

        public Boolean GetGit(SciterElement el, SciterValue[] args, out SciterValue result)
        {
            result = new SciterValue(values =>
            {
                // Create the git runner
                dynamic git = ProcessRunner.Create(Program.GitPath, "LANG=en_US.UTF-8", "PAGER=cat");

                // Invoke it with the passed arguments
                String output = git(String.Join(' ', values.Select(v => v.Get(""))));

                // Return the output
                return new SciterValue(output);
            });
            return true;
        }

        public Boolean GetGitPath(SciterElement el, SciterValue[] args, out SciterValue result)
        {
            result = new SciterValue(GitUtillity.GetGitPath());
            return true;
        }
    }
}
#!/bin/sh

# Check if the .NET core SDK is installed
if ! type dotnet > /dev/null; then
    echo "You need to have the .NET Core SDK installed to build JAG"
    exit
fi

# Check if the script is executed from the repository root
if ! [ -e JAG.sln ]; then
    echo "The build script needs to be run from the repository root"
    exit
fi

# Build the application for windows and linux
dotnet publish -r win-x64 -c "Release Windows" /p:TrimUnusedDependencies=true
dotnet publish -r linux-x64 -c "Release Linux" /p:TrimUnusedDependencies=true